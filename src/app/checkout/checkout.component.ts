import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  angForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) {
  }

  ngOnInit(): void {
    // Create default shipment in basket
    // REQUEST:
    // POST /dw/shop/v20_8/baskets/cdcM6iWbN5jyIaaadh5thrJrYj/shipments HTTP/1.1
    // Host: example.com
    // Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
    // Content-Type: application/json; charset=UTF-8
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      street: ['', Validators.required ],
      city: ['', Validators.required ],
      country: ['', Validators.required ],
      pincode: ['', [Validators.required, Validators.pattern("[0-9]{6}")]],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,12}$")]],
      phone: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      billName: ['', Validators.required ],
      billStreet: ['', Validators.required ],
      billCity: ['', Validators.required ],
      billCountry: ['', Validators.required ],
      billPincode: ['', [Validators.required, Validators.pattern("[0-9]{6}")]],
      billEmail: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,12}$")]],
      billPhone: ['', [Validators.required, Validators.pattern("[0-9]{10}")]]
   });
  }

  onSubmit() {
    if(this.angForm.valid) {
      //Add Shipping Address
      // REQUEST:
      // PUT /dw/shop/v20_8/baskets/cdCxIiWbPdiKAaaadhKTtczLvk/shipments/shipmentId123/shipping_address HTTP/1.1
      // Host: example.com
      // Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
      // Content-Type: application/json

      //Add Shipping Method
      // REQUEST:
      // PUT /dw/shop/v20_8/baskets/cdqwkiWbPdFlkaaadh8xRczLvx/shipments/shipmentId1/shipping_method HTTP/1.1
      // Host: example.com
      // Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
      // Content-Type: application/json

      //Add Billing Address
      //PUT /dw/shop/v20_8/baskets/cdTwMiWbOhGJgaaadkIKbj5op9/billing_address HTTP/1.1
      // Host: example.com
      // Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
      // Content-Type: application/json

      // Place order
      // REQUEST:
      // POST /dw/shop/v20_8/orders HTTP/1.1
      // Host: example.com
      // Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
      // Content-Type: application/json
      // {
      //   "basket_id" : "cdxEQiWbNVZZaaaadhKBc35gtp"
      // }
      this.router.navigate(['/thankyou']);
    }
  }

}
