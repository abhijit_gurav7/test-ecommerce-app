import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  addedProduct = [];
  quantityDropdow = [1, 2, 3, 4, 5];

  constructor() { }

  ngOnInit(): void {
    if(sessionStorage.basket != '') {
      let sessionData = JSON.parse(sessionStorage.basket);
      this.addedProduct = sessionData;
      this.addedProduct.forEach((product) => {
          if(!product.variants[0].quantity) {
            product.variants[0].quantity = 1;
          }
          if(!product.variants[0].totalPrice) {
            product.variants[0].totalPrice = product.variants[0].price;
          }
      });
    }
  }

  onQuantityChange(e, item) {
    // Product quantity change
    // REQUEST:
    // PATCH /dw/shop/v20_8/baskets/cd6HwiWbLaZuUaaadgtglhMTrG/items/cdheYiWbLasNkaaadgwMthMTrG HTTP/1.1
    // Host: example.com
    // Authorization: Bearer a5b6eb0d-8312-41a3-88f3-2c53c4507367
    // {
    //   "_resource_state" : "847f9c3c5867f641470b3046aeec31f07757991b792d722e10079926f7a289fb",
    //   "product_id": "IPad2",
    //   "quantity": 1,
    //   "option_items": [
    //     {
    //       "option_id": "Warranty",
    //       "option_value_id": "oneYear"
    //     }
    //   ]
    // }
    let selectedQuantity = e.target.value;
    let price = item.variants[0].price;
    console.log(selectedQuantity, price);
    let totalPrice = price * selectedQuantity;
    this.addedProduct.forEach((product) => {
      if (product.id == item.id) {
        product.variants[0].quantity = selectedQuantity;
        product.variants[0].totalPrice = totalPrice;
        return;
      }
    });
    sessionStorage.basket = JSON.stringify(this.addedProduct);
  }

  onRemoveProduct(item) {
    // Remove Product from Cart
    // REQUEST:
    // DELETE /dw/shop/v20_8/baskets/ceK4aiWbN132MaaadkYERkclwC/items/cerZoiWbN1K2Yaaadk6URkclwC HTTP/1.1
    // Host: example.com
    // Authorization: Bearer cd669706-3638-4dd1-a8b2-310ab900ca53
    // Content-Type: application/json
    // x-dw-resource-state: 847f9c3c5867f641470b3046aeec31f07757991b792d722e10079926f7a289fb

    let i = 0;
    this.addedProduct.forEach((product) => {
      if (product.id == item.id) {
        this.addedProduct.splice(i, 1);
        return;
      }
      i++;
    });
    sessionStorage.basket = JSON.stringify(this.addedProduct);
  }
}
