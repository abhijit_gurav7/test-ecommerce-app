import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

declare var $: any;

@Component({
  selector: 'app-products',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  selectedCategories = [];
  selectedFilters = [];
  categorieData;
  productData = [];
  filterData;
  articles;
  addedProduct = [];
  addedProductToCartMsg = "Added Product To Cart Successfully!";
  mdlSampleIsOpen : boolean = false;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getProducts().subscribe((data)=>{
	// Get A Categories : 	/catalogs/{catalog_id}
	// REQUEST:
	// POST /s/-/dw/data/v20_8/catalogs/main-catalog/category_search HTTP/1.1
	// Host: example.com
	// Authorization: Bearer a5b6eb0d-8312-41a3-88f3-2c53c4507367
	// Accept: application/json
      let categories = data['categories'];
	  let filters = data['rankings'];
      this.categorieData = categories;
	  this.filterData = filters;
	// Get Category Products : /catalogs/{catalog_id}/categories/{category_id}/category_product_assignment_search
	// REQUEST:
	// POST /s/-/dw/data/v20_8/catalogs/SampleCatalog/categories/SampleCategory/category_product_assignment_search HTTP/1.1
	// Host: example.com
	// Authorization: Bearer a5b6eb0d-8312-41a3-88f3-2c53c4507367
	// Accept: application/json
      for (let category of categories) {
        let productObj = category.products;
        for (let product of productObj) {
          this.productData.push(product);
        }
      }
    });

    if(sessionStorage.basket != '') {
		let sessionData = JSON.parse(sessionStorage.basket);
		this.addedProduct = sessionData;
	}

	$(document).ready(function(){
        $("#showFilter").on('click', function(e) {
			$('.sidenav').removeClass('d-none').addClass('d-block');
		});
		
		$("#closeSidenav").on('click', function(e) {
			$('.sidenav').addClass('d-none').removeClass('d-block');;
		});
    });
  }

  onAddToCart(e, item) {
	// Create Basket: /baskets
	// REQUEST:
	// POST /dw/shop/v20_8/baskets HTTP/1.1
	// Host: example.com
	// Authorization:Bearer eyJfdiI6IjXXXXXX.eyJfdiI6IjEiLCJleHAXXXXXXX.-d5wQW4c4O4wt-Zkl7_fiEiALW1XXXX 
	// Content-Type: application/json
	// Content-Length: 0

	// Add product to basket
	// REQUEST:
	// POST /dw/shop/v20_8/baskets/this/items HTTP/1.1
	// Host: example.com
	// Authorization: Bearer af7f5c90-ffc1-4ea4-9613-f5b375b7dc19
	// Content-Type: application/json
	// [{
	// "product_id" : "cup-white",
	// "quantity" : 1.00,
	// "c_anycustomproperty": "CustomPropertyValue"
	// }]
	this.mdlSampleIsOpen = true;
	if(this.addedProduct.length > 0) {
		let productExist = false;
		this.addedProduct.forEach(data => {
			if(data.id == item.id) {
				productExist = true;
			}
		});
		if(!productExist) {
			this.addedProductToCartMsg = "Added Product To Cart Successfully!";
			this.addedProduct.push(item);
		} else {
			this.addedProductToCartMsg = "Product Already Present In Cart!";
		}
	} else {
		this.addedProductToCartMsg = "Added Product To Cart Successfully!";
		this.addedProduct.push(item)
	}
	sessionStorage.basket = JSON.stringify(this.addedProduct);
  }

  onCheckboxChange(e){
	// Get Category Products : /catalogs/{catalog_id}/categories/{category_id}/category_product_assignment_search
	// REQUEST:
	// POST /s/-/dw/data/v20_8/catalogs/SampleCatalog/categories/SampleCategory/category_product_assignment_search HTTP/1.1
	// Host: example.com
	// Authorization: Bearer a5b6eb0d-8312-41a3-88f3-2c53c4507367
	// Accept: application/json

    if (e.target.checked) {
      this.selectedCategories.push(e.target.value);
    } else {
      let i: number = 0;
      this.selectedCategories.forEach((selectedId) => {
        if (selectedId == e.target.value) {
          this.selectedCategories.splice(i, 1);
          return;
        }
        i++;
      });
	}
	this.categoryFilter(this.selectedCategories, this.selectedFilters);
  }

  onCheckboxFilterChange(e){
	// OCAPI for filter
	// REQUEST:
	// GET /dw/shop/v20_8/product_search?q=shoes&refine_1=cgid=womens&refine_2=c_refinementColor=Black&refine_3=price=(0..500)&sort=top-sellers&count=5&expand=prices HTTP/1.1
	// Host: example.com
	// Accept: application/json
    if (e.target.checked) {
		this.selectedFilters = [];
      	this.selectedFilters.push(e.target.value);
    } else {
      let i: number = 0;
      this.selectedFilters.forEach((selectedId) => {
        if (selectedId == e.target.value) {
          this.selectedFilters.splice(i, 1);
          return;
        }
        i++;
      });
	}
	this.categoryFilter(this.selectedCategories, this.selectedFilters);
  }

  openModal(open : boolean) : void {
	this.mdlSampleIsOpen = open;
  }

  categoryFilter(selectedCat,selectedFlt) {
	this.apiService.getProducts().subscribe((data)=>{
		let filteredData = [];
		let originalData = [];
		let categories = data['categories'];
		for (let category of categories) {
			let productObj = category.products;
			let categoryID : String = category.id.toString();
			if(selectedCat.indexOf(categoryID) !== -1) {
				console.log(categoryID, selectedCat.indexOf(category.id));
				for (let product of productObj) {
					filteredData.push(product);
					originalData.push(product);
				}
			} else {
				console.log(categoryID, selectedCat.indexOf(category.id));
				for (let product of productObj) {
					originalData.push(product);
				}
			}
			
		}

		if(selectedCat.length > 0 || filteredData.length > 0) {
			this.productData = filteredData;
		} else {
			this.productData = originalData;
		}

		for (let filter of this.filterData) { 
			if(selectedFlt.indexOf(filter.ranking) !== -1) {
				for (let filterProduct of filter.products) {
					let i: number = 0;
					for (let product of this.productData) { 
						if(product.id == filterProduct.id) {
							this.productData.splice(i, 1);
						}
						i++;
					}
				}
			}
		}

	  });
  }

}