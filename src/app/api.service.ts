import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_KEY = 'YOUR_API_KEY';

  constructor(private httpClient: HttpClient) { }

  public getProducts(){
    return this.httpClient.get(`https://stark-spire-93433.herokuapp.com/json`);
  }
}
